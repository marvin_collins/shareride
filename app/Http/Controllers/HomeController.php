<?php

namespace App\Http\Controllers;

use Cytonn\Models\OfferRide;
use Cytonn\Models\Ride;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home')
            ->withRides(count(Ride::where('user_id', Auth::user()->id)->get()))
            ->withOffers(count(OfferRide::where('user_id', Auth::user()->id)->get()));
    }
}
