<?php

namespace App\Http\Controllers;


use App\Mail\Shareride;
use App\User;
use Cytonn\Models\OfferRide;
use Cytonn\Models\Ride;
use Cytonn\Repo\RideRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Riazxrazor\LaravelSweetAlert\LaravelSweetAlert;

class RideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rides.index')
            ->withRides(Ride::with(['offer.user'])->where('user_id', Auth::user()->id)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ride  $ride
     * @return \Illuminate\Http\Response
     */
    public function show($offer_id)
    {
        return view('rides.show')
        ->withOffer(OfferRide::with(['rides.user','user'])->findorfail($offer_id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ride  $ride
     * @return \Illuminate\Http\Response
     */
    public function edit(Ride $ride)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ride  $ride
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ride $ride)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ride  $ride
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ride $ride)
    {
        $ride->delete();

        LaravelSweetAlert::setMessageSuccess('Ride canceled successfully');

        return redirect()->back();
    }

    public function rides()
    {
        return view('rides.get-rides')
            ->withRides(OfferRide::with(['user','rides'])->where('user_id','!=',Auth::user()->id)
                ->has('user')->get());
    }

    public function getRide($ride_id)
    {
        $check_space = (new RideRepo())->checkCapacity(OfferRide::findorfail($ride_id));

        if ($check_space){

            Ride::create(['offer_ride_id' => $ride_id,'user_id' => Auth::user()->id,'status' =>Ride::ACTIVE]);
            $data['name'] = Auth::user()->name;
            $data['ride_id'] = $ride_id;
            $data['message'] = 'Your ride was successfully booked.';

            Mail::to(Auth::user()->email)->send(new Shareride($data));

            LaravelSweetAlert::setMessageSuccess('Ride booked successfully');

            return redirect()->route('rides.show', $ride_id);
        }

        LaravelSweetAlert::setMessageError('No Space Available');

        return redirect()->back();
    }
}
