<?php

namespace App\Http\Controllers;

use App\Http\Requests\OfferRideRequest;
use Cytonn\Models\OfferRide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Riazxrazor\LaravelSweetAlert\LaravelSweetAlert;

class OfferRideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('offer-ride.index')
            ->withOffers(OfferRide::with(['rides.user'])->where('user_id', Auth::user()->id)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('offer-ride.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OfferRideRequest $request)
    {
        //check if the
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $data['status'] = OfferRide::ACTIVE;

        OfferRide::create($data);

        LaravelSweetAlert::setMessageSuccess('Ride added successfully');

        return redirect()->route('offer-ride.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OfferRide  $offerRide
     * @return \Illuminate\Http\Response
     */
    public function show($offer_id)
    {
        return view('offer-ride.show')
            ->withOffer(OfferRide::with(['rides.user'])->findorfail($offer_id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OfferRide  $offerRide
     * @return \Illuminate\Http\Response
     */
    public function edit(OfferRide $offerRide)
    {
        return view('offer-ride.edit')
            ->withOffer($offerRide);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OfferRide  $offerRide
     * @return \Illuminate\Http\Response
     */
    public function update(OfferRideRequest $request, OfferRide $offerRide)
    {
        $offerRide->update($request->all());

        LaravelSweetAlert::setMessageSuccess('Ride updated successfully');

        return redirect()->route('offer-ride.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OfferRide  $offerRide
     * @return \Illuminate\Http\Response
     */
    public function destroy(OfferRide $offerRide)
    {
        OfferRide::destroy($offerRide->id);

        LaravelSweetAlert::setMessageSuccess('Ride deleted successfully');

        return redirect()->route('offer-ride.index');
    }
}
