<?php

namespace App;

use Cytonn\Models\OfferRide;
use Cytonn\Models\Ride;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public function ride()
    {
        return $this->hasMany(Ride::class);
    }

    public function offerRide()
    {
        return $this->hasMany(OfferRide::class);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
