@component('mail::message')
Dear {{ $details['name'] }},<br>

{{ ucfirst($details['message']) }}

@component('mail::button', ['url' => url('/offer-ride/'.$details['ride_id'])])
View Ride
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
