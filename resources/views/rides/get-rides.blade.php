@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-primary">
                    <div class="panel-heading clearfix"><strong>List of Available Rides</strong>
                        <a href="{{ route('offer-ride.create') }}" class="btn pull-right btn-warning">Offer Ride</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-boarded">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Origin</th>
                                <th>Destination</th>
                                <th>Driver</th>
                                <th>Capacity</th>
                                <th>Space Available</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rides as $ride)
                                @if($ride->capacity > count($ride->rides))
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ ucwords($ride->origin) }}</td>
                                    <td>{{ ucwords($ride->destination) }}</td>
                                    <td>{{ ucwords($ride->user->name) }}</td>
                                    <td>{{ $ride->capacity }}</td>
                                    <td>{{ count($ride->rides) }}</td>
                                    <td class="text-center">

                                        <a href="{{ url('/ride/'.$ride->id) }}" class="btn btn-success">Get A Ride</a>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        <a href="{{ url()->previous() }}" class="btn btn-warning">Back</a>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
