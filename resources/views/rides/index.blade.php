@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-primary">
                    <div class="panel-heading clearfix">My Booked Rides
                        <a href="{{ url('/get-ride') }}" class="btn pull-right btn-warning">Get A Ride</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-boarded">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Origin</td>
                                <td>Destination</td>
                                <td>Driver</td>
                                <td class="text-center">Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rides as $ride)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ ucwords($ride->offer->origin) }}</td>
                                    <td>{{ ucwords($ride->offer->destination) }}</td>
                                    <td>{{ ucwords($ride->offer->user->name) }}</td>
                                    <td class="text-center">
                                        <form action="{{ route('rides.destroy', $ride->id) }}" method="post">
                                        <a href="{{ route('rides.show', $ride->offer->id) }}" class="btn btn-success">View</a>

                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="submit" class="btn btn-danger" value="Cancel">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <a href="{{ url()->previous() }}" class="btn btn-warning">Back</a>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
