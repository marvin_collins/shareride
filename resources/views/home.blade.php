@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Welcome {{ Auth::user()->name }}</div>

                <div class="panel-body">
                    <div class="col-sm-6">
                        <div class="well well-lg">
                            <h4 class="text-center">Total Ride Offered <strong>{{ $offers }}</strong></h4>

                            <a href="{{ route('offer-ride.create') }}" class="btn center-block btn-primary"> Offer Ride</a>

                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="well well-lg">
                            <h4 class="text-center">Total Ride Booked <strong>{{ $rides }}</strong></h4>

                            <a href="{{ url('get-ride') }}" class="btn center-block btn-primary"> Get A Ride</a>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
