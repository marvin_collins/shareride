@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading clearfix">List of Featured Riders
                        <a href="{{ route('offer-ride.create') }}" class="btn pull-right btn-warning">Offer Ride</a>
                    </div>

                    <div class="panel-body">
                        <div class="col-sm-6"><strong>Driver : </strong> {{ ucwords($offer->user->name) }}</div>
                        <div class="col-sm-6"><strong>Capacity/Space Available : </strong> {{ $offer->capacity }}/{{count($offer->rides)}}</div>
                        <div class="col-sm-6"><strong>Origin : </strong>{{ ucwords($offer->origin) }}</div>
                        <div class="col-sm-6"><strong>Destination : </strong>{{ ucwords($offer->destination) }}</div>
                        <div class="row">
                            <hr>
                        </div>
                        <table class="table table-boarded">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Name</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($offer->rides as $rider)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ ucwords($rider->user->name) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <a href="{{ url()->previous() }}" class="btn btn-warning">Back</a>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
