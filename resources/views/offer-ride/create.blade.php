@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">Add Ride</div>

                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('offer-ride.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('origin') ? ' has-error' : '' }}">
                                <label for="origin" class="col-md-4 control-label">Origin</label>

                                <div class="col-md-6">
                                    <input id="origin" type="text" class="form-control" name="origin" value="{{ old('origin') }}" required autofocus>

                                    @if ($errors->has('origin'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('origin') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('destination') ? ' has-error' : '' }}">
                                <label for="destination" class="col-md-4 control-label">Destination</label>

                                <div class="col-md-6">
                                    <input id="destination" type="text" class="form-control" name="destination" value="{{ old('destination') }}" required>

                                    @if ($errors->has('destination'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('destination') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('capacity') ? ' has-error' : '' }}">
                                <label for="capacity" class="col-md-4 control-label">Vehicle Capacity</label>

                                <div class="col-md-6">
                                    <input id="capacity" type="number" min="1" class="form-control" name="capacity" value="{{ old('capacity') }}" required>

                                    @if ($errors->has('capacity'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('capacity') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <a href="{{ url()->previous() }}" class="btn btn-warning">Back</a>

                                    <button type="submit" class="pull-right btn btn-primary">
                                        Add Ride
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
