@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-primary">
                    <div class="panel-heading clearfix">My Rides
                        <a href="{{ route('offer-ride.create') }}" class="btn pull-right btn-warning">Add Ride</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-boarded">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Origin</td>
                                <td>Destination</td>
                                <td>Capacity</td>
                                <td>Space Available</td>
                                <td>Status</td>
                                <td class="text-center">Action</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($offers as $offer)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ ucwords($offer->origin) }}</td>
                                    <td>{{ ucwords($offer->destination) }}</td>
                                    <td>{{ $offer->capacity }}</td>
                                    <td>{{ count($offer->rides) }}</td>
                                    <td>{{ count($offer->rides) >= $offer->capacity ? 'Full' : ucwords($offer->status) }}</td>
                                    <td class="text-center">
                                        <form action="{{ route('offer-ride.destroy', $offer->id) }}" method="post">
                                        <a href="{{ route('offer-ride.edit', $offer->id) }}" class="btn btn-success">Edit</a>
                                        <a href="{{ route('offer-ride.show', $offer->id) }}" class="btn btn-primary">Show</a>
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <input type="submit" class="btn btn-danger" value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <a href="{{ url()->previous() }}" class="btn btn-warning">Back</a>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
