<?php

namespace Cytonn\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class OfferRide extends Model
{
    const ACTIVE = 'available';
    const FULL = 'unavailable';
    protected $fillable = ['user_id','origin','destination','capacity','status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rides()
    {
        return $this->hasMany(Ride::class);
    }
}
