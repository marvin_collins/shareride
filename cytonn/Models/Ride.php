<?php

namespace Cytonn\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    const ACTIVE = 'booked';
    const CANCELED = 'canceled';

    protected $fillable = ['offer_ride_id','user_id','status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function offer()
    {
        return $this->belongsTo(OfferRide::class, 'offer_ride_id', 'id');
    }
}
