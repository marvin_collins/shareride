<?php
namespace Cytonn\Repo;
use Cytonn\Models\OfferRide;

/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 8/14/17
 * Time: 1:24 AM
 */
class RideRepo
{
    public function checkCapacity(OfferRide $offer)
    {
        $booked_spaces = count($offer->rides);

        if ($offer->capacity <= $booked_spaces){

            return false;
        }

        return true;
    }
}